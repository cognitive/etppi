package rtcw;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import javax.swing.BorderFactory;

class StatusBarPanel extends JPanel 
{

	JTextArea textArea = null;

	public StatusBarPanel () {

		super();
		Border bevelLowered = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		textArea = new JTextArea ("welcome to ETSPI.");

		this.setLayout( new BorderLayout());
		this.setBorder(	bevelLowered );

		textArea.setEditable(false);
		textArea.setBackground ( this.getBackground ());
		this.add( textArea, BorderLayout.SOUTH );

	}

	public void setText( String text ) {
		textArea.setText( text );
	}
}
