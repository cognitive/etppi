package rtcw;

// Frame related
import javax.swing.JFrame;
import java.awt.HeadlessException;

// Layout related
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.*;

// Widget related
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JProgressBar;

import java.util.Vector;


class MainFrame extends JFrame  
{

	ActionManager mgr =	new ActionManager( this );

	Box main =			new Box(BoxLayout.Y_AXIS);
	//Box panel1 =		new Box(BoxLayout.X_AXIS);
	Box panel2 =		new Box(BoxLayout.X_AXIS);
	Box panel3 =		new Box(BoxLayout.X_AXIS);
	Box panel4 =		new Box(BoxLayout.X_AXIS);
	Box panel5 =		new Box(BoxLayout.X_AXIS);

	JButton selectBt =	new JButton("Select RTCW ET stats directories");
	JButton clearBt =	new JButton("Clear");
//	JButton loadBt =	new JButton("Load");
	JButton saveBt =	new JButton("Save");
	JButton parseBt =	new JButton("Parse");
	
	Vector WidgetsToEnable = new Vector();

	//JTextField nameTF =			new JTextField("player name here");
	JList fileList =			new JList();
	JLabel countFilesLB =		new JLabel("0 files selected");
	//JLabel nameLB =				new JLabel("Name");
	JScrollPane scrollPane =	null;

	JProgressBar progress = null;

	boolean firstFSelection = true;



	
	/**
	public void enableWindow( boolean enabled ) {	
		clearBt.setEnabled(enabled);
		saveBt.setEnabled(enabled);
		parseBt.setEnabled(enabled);
		selectBt.setEnabled(enabled);
		loadBt.setEnabled(enabled);
		nameTF.setEnabled(enabled);
		fileList.setEnabled(enabled);
		countFilesLB.setEnabled(enabled);
		nameLB.setEnabled(enabled);
	}
	**/


	public MainFrame(String title) throws HeadlessException {
		
		super ( title );
			
		Border bevelLowered = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		Border mo_title =	BorderFactory.createTitledBorder(bevelLowered, "Stats file selection");
		main.setBorder(mo_title);

		main.setBorder( 
			BorderFactory.createCompoundBorder(
				main.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		
		
		//panel1.add(nameLB);
		//panel1.add(Box.createHorizontalStrut(10));
		//panel1.add( nameTF );

		// adding button
		panel2.add(selectBt);
		selectBt.addActionListener(mgr);
		selectBt.setActionCommand("select");

		//main.add(panel1);
		//main.add(Box.createVerticalStrut(10));
		main.add(panel2);
		main.add(Box.createVerticalStrut(10));
		main.add(panel3);
		main.add(Box.createVerticalStrut(10));
		main.add(panel4);
		main.add(Box.createVerticalStrut(10));
		main.add(panel5);

		this.getContentPane().add(main);

		// hook to initialize with previous settings
		mgr.windowShownFirstTime();
		return;
	}

//	public String getName () {
//		return nameTF.getText();
//	}

	

	
	public void updateFileList ( Vector dirVector, Vector filVector) {

		fileList.setListData( dirVector );
		countFilesLB.setText( Integer.toString( filVector.size() ) +  " files selected" );

		// Added only after 1st selection
		if ( firstFSelection ) {
			scrollPane = new JScrollPane(fileList);
			panel3.add(scrollPane);

			panel4.add( countFilesLB ); 
			panel4.add(Box.createHorizontalStrut(10));

			panel4.add( clearBt );
			clearBt.addActionListener(mgr);
			clearBt.setActionCommand("clear");
		
/*			panel4.add(Box.createHorizontalStrut(10));

			panel4.add( loadBt );
			loadBt.addActionListener(mgr);
			loadBt.setActionCommand("load");
*/ 
			panel4.add(Box.createHorizontalStrut(10));

			panel4.add( saveBt );
			saveBt.addActionListener(mgr);
			saveBt.setActionCommand("save");

			panel4.add(Box.createHorizontalStrut(10));

			panel4.add( parseBt );
			parseBt.addActionListener(mgr);
			parseBt.setActionCommand("parse");

			firstFSelection = false;
		}

		this.pack();
	}

	// First call only add ProgressBar 
	// 0, 0 set ProgressBar in undetermined state
	// else give ProgressBar the right display
	
	public void  updateProgressBar (int currValue, int maxValue) {

		// First Call
		if ( progress == null )	{
			progress = new JProgressBar();
			progress.setIndeterminate(true);
			panel5.add( progress );
			this.pack();
		}
		
		// call with 0 and 0
		if ( currValue == 0 && maxValue == 0) {
			progress.setIndeterminate(true);
			return;
		}
		
		// else 
		progress.setStringPainted(true);
		if ( progress.getMaximum() != maxValue) {
			progress.setMaximum(maxValue);
		}
		if ( progress.isIndeterminate() )	{
			progress.setIndeterminate(false);
		}
		
		progress.setValue(currValue);
		return;
	}

	public void enableWindow( boolean enabled ) {	

		GuiUtil.getInstance().setFrameEnabled( this, enabled );
	}
}
