/* Auteur: Denis ETIENNE
 * Date:   22/06/2003
 * 
 * Commentaires en anglais, bient�t en francais
 * Utilisation intensive de javax.swing.Box avec des Struts
 *	
 * Pr�requis:    sdk de java2se v1.4.2 	
 * Compilation:  javac ETStat.java
 * Execution:    java ETStat.java
 *
 * Required:	 java2se 1.4.2 sdk 		
 * Compile with: javac ETStat.java
 * Run with:     java ETStat
 *
 * Commentaires constructifs: 	denis.etienne@club-internet.fr
 * Valuable comments:			denis.etienne@club-internet.fr
 */

/* Fixed : Long player name
 * Fixed: Fixer team damage sur 5 caracteres (scoreCPattern) dans ParseEngine.java
 * example: 172034.txt dans 2003.06.15
 * TODO: Empecher le Parse Bouton si il n y a pas de Selection ??
 * TODO: Trier les noms de repertoire par odre alphabetique et empecher les doublons
 * Fixed: Revoir Hard Coded Limit (999)
 */

package rtcw;


/* RadioButton needs ButtonGroup */

/* JComboBox needs Vector */

import rtcw.MainFrame;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.UIManager;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
 

public class ETStat
{
	static String appName = "ET-PPI";
	static String versionNumber = "1.3";
	static MsgOutput o = new MsgOutput( MsgOutput.WARNING, "RTCW.ETStat" );
	
	// Update enable ?
	static boolean disablecheck = false;

	static public String getAppName() {
		return new String(appName);
	}
	static public String getAppNameVersion() {
		return new String(getAppName()+" "+versionNumber);
	}

	static public int getVersionMajor () {
		String [] numbers = versionNumber.split("\\.");
		return Integer.parseInt( numbers[0] );
	}

	static public int getVersionMinor () {
		String [] numbers = versionNumber.split("\\.");
		return Integer.parseInt( numbers[1] );
	}

	public static void main(String[] args)
	{

		/* Try to apply default Look and Feel */
		try
		{
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
		} catch (Exception e) {
			System.out.println("Error setting LAF: " + e);
		}
		
		//Frame decoration
		JFrame.setDefaultLookAndFeelDecorated(true);
        //JDialog.setDefaultLookAndFeelDecorated(true)

		for (int i=0; i<args.length ;i++ ) {
			if (args[i].equals("-nocheck")) disablecheck = true;
		}


		// Checking for update
		if (!disablecheck) {
			Update.start();
		}


		/* Create the top-level container and add contents to it. */
		MainFrame mframe = new MainFrame("Stats files selection  "+ETStat.getAppName());
		

		/* Finish setting up the frame, and show it. */
		mframe.addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			});
		mframe.pack();
		mframe.setVisible(true);
		return;
	}

		
	/** Returns an ImageIcon, or null if the path was invalid. */
    public static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = ETStat.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            o.warning("Couldn't find file: " + path);
            return null;
        }
    }
}




