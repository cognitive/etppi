package rtcw;

import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JColorChooser;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.GridLayout;
import java.util.Vector;

// jeudi 17 juillet 2003
// Interface 
//  getColors renvoie un vecteur de couleur correspondant au choix
//  setColors recoit un vecteur de couleur et met les boutons aux couleurs choisies

class ColorChooserPanel extends JPanel implements ActionListener,
                                          ChangeListener {

	// position des couleurs dans le Vector result de getColors
	public static final int NOGRADIENTCOLOR1	= 0;
	public static final int NOGRADIENTCOLOR2	= 1;
	public static final int GRADIENTCOLORHOT1	= 2;
	public static final int GRADIENTCOLORCOLD1	= 3;
	public static final int GRADIENTCOLORHOT2	= 4;
	public static final int GRADIENTCOLORCOLD2	= 5;
	public static final int VALUESCOLOR			= 6;
	public static final int BACKGROUNDCOLOR		= 7;

	// Ttes les options
	//boolean textAliasing =		true;
	//boolean drawMargins =		false;
	//boolean useGradient =		true;
	//-Color singleColor1 =		Color.yellow; 
	//-Color singleColor2 =		Color.orange;
	//-Color gradientHotColor1 =	Color.red;
	//-Color gradientColdColor1 =	Color.orange;
	//-Color gradientHotColor2 =	Color.red;
	//-Color gradientColdColor2 =	Color.orange;
	//Color backGroundColor = Color.lightGray;
	//Font  font				= new Font("Dialog", Font.BOLD, 9); 

	JButton nogradientcolor1=	new JButton("No Gradient Axis");
	JButton nogradientcolor2=	new JButton("No Gradient Allies");
	JButton gradientcolorh1=	new JButton("Gradient Axis Hot");
	JButton gradientcolorc1=	new JButton("Gradient Axis Cold");
	JButton gradientcolorh2=	new JButton("Gradient Allies Hot");
	JButton gradientcolorc2=	new JButton("Gradient Allies Cold");
	JButton valuescolor=		new JButton("Values Color");
	JButton backgroundcolor=    new JButton("Background Color");
	JCheckBox gradientCB=		new JCheckBox("Use Gradients", true);
	
	public Vector getColors () {
		Vector colorVector = new Vector();
		colorVector.add(nogradientcolor1.getBackground());
		colorVector.add(nogradientcolor2.getBackground());
		colorVector.add(gradientcolorh1.getBackground());
		colorVector.add(gradientcolorc1.getBackground());
		colorVector.add(gradientcolorh2.getBackground());
		colorVector.add(gradientcolorc2.getBackground());
		colorVector.add(valuescolor.getBackground());
		colorVector.add(backgroundcolor.getBackground());
		return colorVector;
	}


	public boolean setColors ( Vector colors) {
	
		if (colors.size() != 8) return false;
		Object [] colorsa = colors.toArray();
		nogradientcolor1.setBackground( (Color)colorsa[NOGRADIENTCOLOR1]);
		nogradientcolor2.setBackground( (Color)colorsa[NOGRADIENTCOLOR2]);
		gradientcolorh1.setBackground(  (Color)colorsa[GRADIENTCOLORHOT1]);
		gradientcolorc1.setBackground(  (Color)colorsa[GRADIENTCOLORCOLD1]);
		gradientcolorh2.setBackground(  (Color)colorsa[GRADIENTCOLORHOT2]);
		gradientcolorc2.setBackground(  (Color)colorsa[GRADIENTCOLORCOLD2]);
		valuescolor.setBackground(		(Color)colorsa[VALUESCOLOR]);
		backgroundcolor.setBackground(  (Color)colorsa[BACKGROUNDCOLOR]);
		return true;
	}
	

	public boolean get_useGradient() {
		return gradientCB.isSelected();
	}

	public void set_useGradient( boolean useGradient) {
		gradientCB.setSelected(useGradient);
		updateButtonsEnabled (useGradient);
	}

	//////////////////////////////////////////////////////////////////////////


	public ColorChooserPanel() {
		super ();

		//this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.setLayout(new GridLayout(10,1));
		this.add( nogradientcolor1 );
		this.add( nogradientcolor2 );
		this.add( gradientCB );
		this.add( gradientcolorh1 );
		this.add( gradientcolorc1 );
		this.add( gradientcolorh2 );
		this.add( gradientcolorc2 );
		this.add( new JLabel() );
		this.add( valuescolor );
		this.add( backgroundcolor );

		nogradientcolor1.addActionListener(this);
		nogradientcolor2.addActionListener(this);
		gradientcolorh1.addActionListener(this);
		gradientcolorc1.addActionListener(this);
		gradientcolorh2.addActionListener(this);
		gradientcolorc2.addActionListener(this);
		valuescolor.addActionListener(this);
		backgroundcolor.addActionListener(this);
		gradientCB.addActionListener(this);
	
		updateButtonsEnabled (gradientCB.isSelected());
	}
	


    public void actionPerformed(ActionEvent e) {

		Object source = e.getSource();

		if ( source instanceof JButton) {
			Color newColor = 
				JColorChooser.showDialog( this,
										   "Choose Color",
										   Color.lightGray);

			if (newColor != null) {
					JButton button = (JButton)source;
					button.setBackground(newColor);
			}
			
		}
		
		if ( source instanceof JCheckBox) {
			JCheckBox cb = (JCheckBox)source;
			updateButtonsEnabled (cb.isSelected());

		}
    }

	private void updateButtonsEnabled ( boolean use_gradients ) {
	
			nogradientcolor1.setEnabled( !use_gradients );
			nogradientcolor2.setEnabled( !use_gradients );
			gradientcolorh1.setEnabled( use_gradients );
			gradientcolorc1.setEnabled( use_gradients );
			gradientcolorh2.setEnabled( use_gradients );
			gradientcolorc2.setEnabled( use_gradients );
	}


    public void stateChanged(ChangeEvent e) {
//        Color newColor = tcc.getColor();
//        banner.setForeground(newColor);
    }

}