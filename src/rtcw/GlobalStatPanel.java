package rtcw;

import javax.swing.BorderFactory;
import javax.swing.border.*;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.text.DecimalFormat;

class GlobalStatPanel extends JPanel
{
	Color valueColor = Color.blue;

	Box main =			new Box(BoxLayout.Y_AXIS);
	
	Box panel1 =		new Box(BoxLayout.Y_AXIS);
	Box panel2 =		new Box(BoxLayout.Y_AXIS);
	Box panel3 =		new Box(BoxLayout.Y_AXIS);
	Box panel4 =		new Box(BoxLayout.Y_AXIS);

	JLabel firstLogLB =		new JLabel("First Log ");
	JLabel lastLogLB  =		new JLabel("Last Log ");
	JLabel roundPlayedLB  =	new JLabel("Round Played ");
	JLabel avgeXPLB  =		new JLabel("Avge XP ");
	JLabel avgeKillLB  =	new JLabel("Avge Kills ");
	JLabel avgeDeathLB  =	new JLabel("Avge Deaths ");
	JLabel avgeHShotLB  =	new JLabel("Avge HShots ");
	JLabel avgeDGLB  =		new JLabel("Avge DmgeGiven ");
	JLabel avgeDRLB  =		new JLabel("Avge DmgeRecvd ");
	JLabel avgeDTLB  =		new JLabel("Avge DmgeTeam ");

	JLabel flogValueLB =	new JLabel("00/00/00");
	JLabel llogValueLB =	new JLabel("00/00/00");
	JLabel rPlayedValueLB =	new JLabel("0");
	JLabel avXPValueLB =	new JLabel("0.0");
	JLabel avKValueLB =		new JLabel("0.0");
	JLabel avDValueLB =		new JLabel("0.0");
	JLabel avHSValueLB =	new JLabel("0.0");
	JLabel avDGValueLB =	new JLabel("0.0");
	JLabel avDRValueLB =	new JLabel("0.0");
	JLabel avDTValueLB =	new JLabel("0.0");
	
	JLabel startDLB =		new JLabel("Start Date:");
	JLabel endDateLB  =		new JLabel("End Date:");
	
	public void setDates(String start, String end) {
		flogValueLB.setText(start);
		llogValueLB.setText(end);
	}

	// Versions publiques
	public void setAverage(double Kill, double Death, double HShot, double DG, double DR, double DT ) {
		setAverage(
			doubleToString(Kill),doubleToString(Death),doubleToString(HShot), 
			Integer.toString((int)DG),  Integer.toString((int)DR), Integer.toString((int)DT) );
	}

	public void setRoundPlayed( int rplayed ) {
		setRoundPlayed(Integer.toString(rplayed));
	}

	// Versions privees
	private void setAverage(String Kill, String Death, String HShot, String DG, String DR, String DT ) {
		avKValueLB.setText(Kill);
		avDValueLB.setText(Death);
		avHSValueLB.setText(HShot);
		avDGValueLB.setText(DG);
		avDRValueLB.setText(DR);
		avDTValueLB.setText(DT);
	}

	private void setRoundPlayed( String rplayed ) {
		rPlayedValueLB.setText(rplayed);
	}

	public GlobalStatPanel ( ) {

		Border insideSpaceBorder=	BorderFactory.createEmptyBorder(8,12,8,12);
		Border bevelLowered =		BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		Border mo_title =			BorderFactory.createTitledBorder(bevelLowered, "Overall Statistics");

		this.setBorder( BorderFactory.createCompoundBorder(
						  mo_title,
						  insideSpaceBorder));

		this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));

		panel1.add( firstLogLB);
		panel1.add( lastLogLB );
		panel1.add( roundPlayedLB );
		panel1.add( new JLabel(" ") );
		panel1.add( new JLabel(" ") );
		panel1.add( new JLabel(" ") );


		flogValueLB.setForeground( valueColor );
		llogValueLB.setForeground( valueColor );
		rPlayedValueLB.setForeground( valueColor );
		panel2.add( flogValueLB );
		panel2.add( llogValueLB );
		panel2.add( rPlayedValueLB);
		panel2.add( new JLabel(" ") );
		panel2.add( new JLabel(" ") );
		panel2.add( new JLabel(" ") );

		//panel3.add( avgeXPLB );
		panel3.add( avgeKillLB );
		panel3.add( avgeDeathLB );
		panel3.add( avgeHShotLB );
		panel3.add( avgeDGLB );
		panel3.add( avgeDRLB );
		panel3.add( avgeDTLB );

		//avXPValueLB.setForeground( Color.blue );
		avKValueLB.setForeground ( valueColor );
		avDValueLB.setForeground ( valueColor );
		avHSValueLB.setForeground( valueColor );
		avDGValueLB.setForeground( valueColor );
		avDRValueLB.setForeground( valueColor );
		avDTValueLB.setForeground( valueColor );
		
		//panel4.add( avXPValueLB );
		panel4.add( avKValueLB );
		panel4.add( avDValueLB );
		panel4.add( avHSValueLB );
		panel4.add( avDGValueLB );
		panel4.add( avDRValueLB );
		panel4.add( avDTValueLB );
		
		this.add(panel1);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel2);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel3);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel4);

		

//		Round.refreshAverage();
//		avKValueLB.setText(doubleToString(Round.get_avgkill()));
//		avDValueLB.setText(doubleToString(Round.get_avgdeath()));
//		avHSValueLB.setText(doubleToString(Round.get_avghshot()));
//		avDGValueLB.setText(Integer.toString((int)Round.get_avgDG()));
//		avDRValueLB.setText(Integer.toString((int)Round.get_avgDR()));
//		avDTValueLB.setText(Integer.toString((int)Round.get_avgDT()));


//		this.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {
//                QueryFrame qf = (QueryFrame)e.getSource();
//				qf.mgr.queryFrameClosing();
//            }
//        });
//		
		//this.enableWindow(false);
		return;
	}

	public String doubleToString ( double d ) {

		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits( 1 );
		df.setMinimumFractionDigits( 1 );
		return df.format( d );
	}


}
