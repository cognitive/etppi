package rtcw;

// Frame related
import javax.swing.JFrame;
import java.awt.HeadlessException;
import java.awt.Color;


// Widget related
import java.awt.event.*;

class QueryFrame extends JFrame  
{

	Color valueColor =		Color.blue;
	Color titleColor =      Color.blue;

	public ActionManager mgr =	null;


	public QueryFrame(String title, ActionManager am) throws HeadlessException {
		
		super ( title );
			
		this.mgr = am;

		QueryPanel qp = new QueryPanel( mgr );
		
		this.setContentPane(qp);

		this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                QueryFrame qf = (QueryFrame)e.getSource();
				qf.mgr.queryFrameClosing();
            }
        });
		
		//this.enableWindow(false);
		return;
	}

	public void enableWindow( boolean enabled ) {	
		GuiUtil.getInstance().setFrameEnabled( this, enabled, true );
	}
		
	
}
