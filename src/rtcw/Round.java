package rtcw;

import java.util.GregorianCalendar;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

class Round 
{
	private static String UNKNOWN_DATE = "00/00/0000";
	private static Vector allRounds = new Vector();
	private static long minEndDate = Long.MAX_VALUE;	
	private static long maxEndDate = Long.MIN_VALUE;	

	private static Hashtable pNamesHash = new Hashtable();
	static public Enumeration getDifferentPlayerNames() {
		return pNamesHash.keys();
	}

	private Vector skills  = new Vector();
	private Vector weapons = new Vector();


	String mapName = null;
	String playerName = null;
	String teamName = null; // AXIS/ALLIES
	String fileName = null;
	long endDate = 0;	

	int exp = 0;
	String rank = null;

	// for this round
	int damageGiven= 0;
	int damageReceived=	0;
	int damageTeam= 0;

	// for previous and this round
	int kill = 0;
	int death = 0;
	int suicide = 0;
	int tk = 0;
	int efficiency = 0;

	
	// for this round only
	int roundkill = 0;
	int rounddeath = 0;
	int roundhshot = 0;

	public void incrkill  ( int incr ) { roundkill  += incr; }
	public void incrdeath ( int incr ) { rounddeath += incr; }
	public void incrhshot ( int incr ) { roundhshot += incr; }

	// average value
	static double avgkill	= 0.0;
	static double avgdeath	= 0.0;
	static double avghshot	= 0.0;
	static double avgDG		= 0.0;
	static double avgDR		= 0.0;
	static double avgDT		= 0.0;
	
	public Round ( String fname) {
		
		Round.allRounds.add( this );
		this.fileName = fname;
	}
	
	static public boolean removeRound(Round r) {
		return Round.allRounds.remove(r);
	}

	// Dereference all existing Round Objects
	public static int reset() {
		int numround = Round.allRounds.size(); 
		Round.allRounds = new Vector();
		return numround;
	}

	public static Vector get_allRounds() {
		return Round.allRounds;
	}
	
	public static String get_maxDate () {
	
		if ( minEndDate <= maxEndDate)	{
			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(maxEndDate * 1000);
			//return DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT).format(cal.getTime());
			return DateFormat.getDateInstance(DateFormat.SHORT).format(cal.getTime());
		}
		else return Round.UNKNOWN_DATE;
	}
	
	public static String get_minDate () {

		if ( minEndDate <= maxEndDate)	{
			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(minEndDate * 1000);
			//return DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT).format(cal.getTime());
			return DateFormat.getDateInstance(DateFormat.SHORT).format(cal.getTime());
		}
		else return Round.UNKNOWN_DATE;
	}

	// KEY

	// end date is unique so we choose it as a key
	public long get_key ( ) {
		return get_endDate();
	}

	static public Round get_roundByKey(long key) {
		int numround = allRounds.size();
		int counter = 0;
		Round r = null;

		while (counter < numround) {
			r =  (Round)allRounds.elementAt( counter );
			if ( r.get_key() == key ) return r;
			counter++;
		}
		return r;
	}

	public long get_endDate () {
		return this.endDate;
	}
	
	public void set_playerName ( String pname ) {
		this.playerName = pname ;
	
		if (!Round.pNamesHash.containsKey(pname)) {
			pNamesHash.put(pname, new Integer(1));
		}
	}

	public String get_playerName ( ) {
		return this.playerName;
	}

	public void set_damageGT ( int given, int team ) {
		this.damageGiven = given;
		this.damageTeam = team;
	}

	public void set_damageR ( int received ) {
		this.damageReceived = received;
	}

	public void set_exp ( int experience, String finalRank ) {
		this.exp = experience;
		this.rank = finalRank;
	}

	public void addSkill ( int stype, int level, int point, int medals ) {
		this.skills.add ( new Skill (stype, level, point, medals ) );
	}

	public Skill get_skill ( int type ) {
	
		Skill rslt = new Skill ( type,0,0,0);
		int counter = 0;
		int length = this.skills.size();

		while ( counter < length ) {
			if ( type == ((Skill)this.skills.elementAt(counter)).type ) {
				return (Skill)this.skills.elementAt(counter);
			}
			counter++;
		}
		return rslt;
	}
	
	public void addWeapon ( Weapon w ) {
		this.weapons.add ( w );
	}

	public Weapon get_weapon ( String weapName ) {

		Weapon rslt = new Weapon( weapName, 0.0, new String(), 0, 0);
		int counter = 0;
		int length = this.weapons.size();

		while ( counter < length ) {
			if ( weapName.equals ( ((Weapon)this.weapons.elementAt(counter)).weapName ) ) {
				return (Weapon)this.weapons.elementAt(counter);
			}
			counter++;
		}
		return rslt;
	}


	//String startDateStr = null;

	public void set_endDate ( long edate ) {
		this.endDate = edate;
		if ( this.endDate < minEndDate ) minEndDate = this.endDate;
		if ( this.endDate > maxEndDate ) maxEndDate = this.endDate;
//		Calendar cal = new GregorianCalendar();
//		cal.setTimeInMillis(this.endDate * 1000);
//		System.out.println ("Round ending at : "+ DateFormat.getDateTimeInstance().format(cal.getTime()));
	}


	public void set_teamName ( String tname ) {
		this.teamName = tname;
	}

	public void set_mapName ( String mname ) {
		this.mapName = mname;
	}

	public void set_killDeathTk( int k, int d, int s, int t, int e) {

		this.kill = k;
		this.death = d;
		this.suicide = s;
		this.tk = t;
		this.efficiency = e;
		//System.out.println (" k d s t e : "+ new Integer( k )+" "+new Integer( d )+" "+new Integer( s )
		//+" "+new Integer( t )+" "+new Integer( e ));
		
	}


	// Average values only accept values
	// calculated for only 1 round
	public static void refreshAverage() {
	
		int maxround = allRounds.size();
		int counter = 0;
		double sumkill  = avgkill  =	0.0;
		double sumdeath = avgdeath =	0.0;
		double sumhshot = avghshot =	0.0;
		double sumDG	= avgDG = 0.0;
		double sumDT	= avgDT = 0.0;
		double sumDR	= avgDR = 0.0;

		while (counter < maxround )	{
			Round r = (Round)allRounds.elementAt(counter);
			sumkill  += r.roundkill;
			sumdeath += r.rounddeath;
			sumhshot += r.roundhshot;
			sumDG += r.damageGiven;
			sumDR += r.damageReceived;
			sumDT += r.damageTeam;
			counter++;
		}

		avgkill  = sumkill  / maxround;
		avgdeath = sumdeath / maxround;
		avghshot = sumhshot / maxround;
		avgDG = sumDG / maxround;
		avgDR = sumDR / maxround;
		avgDT = sumDT / maxround;
	}

	public static double get_avgkill () { return avgkill; }
	public static double get_avgdeath () { return  avgdeath; }
	public static double get_avghshot () { return  avghshot; }
	public static double get_avgDG () { return  avgDG; }
	public static double get_avgDR () { return  avgDR; }
	public static double get_avgDT () { return  avgDT; }

	public int get_roundkill() { return this.roundkill;	}
	public int get_rounddeath() { return this.rounddeath; }
	public int get_roundhshot() { return this.roundhshot; }

	public int get_damageG () {	return this.damageGiven; }
	public int get_damageR () {	return this.damageReceived; }
	public int get_damageT () { return this.damageTeam; }
	public String get_mapName () { return this.mapName; }
	public String get_teamName () { return teamName; }
	public String get_fileName () { return fileName; }
}
