package rtcw;

import javax.swing.JFrame;

class RoundInfoFrame extends JFrame
{
	public RoundInfoFrame ( String title, Round r ) {
		super( title );
		this.setContentPane( new RoundInfoPanel ( r ));
	}
	

}
