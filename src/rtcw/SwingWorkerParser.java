package rtcw;

import java.util.Vector;
import java.util.Enumeration;


class SwingWorkerParser extends SwingWorker
{
	private ActionManager am = null;
	private MsgOutput o = new MsgOutput( MsgOutput.WARNING, "RTCW.SwingWorkerParser");
	private ParseEngine pe = new ParseEngine();
 
	private Vector fileNames = null;

	public void setFilesToParse ( Vector fNames ) {
		this.fileNames = fNames;
		return;
	}
	
	public void setActionManager ( ActionManager actionManager ) {
		this.am = actionManager;
		return;
	}

	
	public Object construct() {

		boolean errorOccured = false;
		int progressValue    = 0;
		int progressMaxValue = fileNames.size();
		
		// parse engine is responsible for cleaning all objects it created
		pe.reset();
		
		long end, start = System.currentTimeMillis();

		am.taskProgressChanged(progressValue, progressMaxValue);

		o.debug("Ready to parse: "+fileNames.size()+" files.");
		for (Enumeration e = fileNames.elements() ; e.hasMoreElements() && !errorOccured ;) {
			String currFileName = (String)e.nextElement();
			
			errorOccured = !pe.ParserEntryPoint( currFileName );
			progressValue++;
			
			if ( progressValue % 20 == 0) {
				am.taskProgressChanged(progressValue, progressMaxValue);
			}

			o.debug ( "file "+currFileName+" has been read."); 
		}
		am.taskProgressChanged(progressMaxValue, progressMaxValue);
		
		end =  System.currentTimeMillis();
		o.info("Parsed in "+Long.toString(end - start)+" ms");
		return null;
	}

	public void finished( ) {
		this.am.parsingThreadFinished( this.fileNames.size() );    
	}

}