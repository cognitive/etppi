package rtcw;

class Skill 
{
	public static final int SELECT_POINTS = 10;
	public static final int SELECT_LEVEL  = 11;
	public static final int SELECT_MEDALS = 12;
	
	public static final int BATTLE_SENSE = 1;
	public static final int LIGHT_WEAPONS = 2;
	public static final int HEAVY_WEAPONS = 3;
	public static final int SIGNALS = 4;
	public static final int COVERT_OPS = 5;
	public static final int ENGINEERING = 6;
	public static final int FIRST_AID = 7;
	
	public int type = 0;
	int level = 0;
	int point = 0;
	int medal = 0;


	public Skill ( int type, int level, int point, int medal ) {
		
		this.type  = type;
		this.level = level;
		this.point = point;
		this.medal = medal;
		
	}

	public double get_value ( int selector ) {
		if ( selector == SELECT_POINTS ) return this.point;
		if ( selector == SELECT_LEVEL ) return this.level;
		if ( selector == SELECT_MEDALS ) return this.medal;
		return 0.0;
	}


	public int get_point() { return this.point; }
	public int get_level() { return this.level; }
	public int get_medal() { return this.medal; }


}
