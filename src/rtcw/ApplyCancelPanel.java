package rtcw;

import java.awt.event.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;


// jeudi 17 juillet 2003
// Interface 
//  getColors renvoie un vecteur de couleur correspondant au choix
//  setColors recoit un vecteur de couleur et met les boutons aux couleurs choisies

class ApplyCancelPanel extends JPanel {

	static MsgOutput	o= new MsgOutput( MsgOutput.INFO, "RTCW.ApplyCancelPanel");

	ImageIcon	applyIcon = ETStat.createImageIcon("MiscIconPack/okay.png");		
	ImageIcon	saveIcon  = ETStat.createImageIcon("MiscIconPack/save32.png");		
	ImageIcon	resetIcon = ETStat.createImageIcon("MiscIconPack/cancel.png");		


	JButton resetBt=	new JButton("Reset",resetIcon);
	JButton applyBt=	new JButton("Apply",applyIcon);
	JButton saveBt=		new JButton("Save",saveIcon);
	JButton closeBt=	new JButton("Close");

	//JButton resetBt=	new JButton(resetIcon);
	//JButton applyBt=	new JButton(applyIcon);
	//JButton saveBt=	new JButton(saveIcon);
	

	public ApplyCancelPanel( ActionListener listener ) {
		super ();

		//this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.setLayout(new GridLayout(2,2,15,15));
		
		resetBt.setHorizontalTextPosition(SwingConstants.RIGHT );
		applyBt.setHorizontalTextPosition(SwingConstants.RIGHT );
		saveBt.setHorizontalTextPosition(SwingConstants.RIGHT );
		//closeBt.setHorizontalTextPosition(int textPosition)
		
		
		this.add( resetBt );
		resetBt.addActionListener(listener);
		resetBt.setActionCommand("reset");
		
		this.add( closeBt );
		closeBt.addActionListener(listener);
		closeBt.setActionCommand("close");
		closeBt.setEnabled(false);

		this.add( applyBt );
		applyBt.addActionListener(listener);
		applyBt.setActionCommand("apply");
		
		this.add( saveBt );
		saveBt.addActionListener(listener);
		saveBt.setActionCommand("save");

	}

}