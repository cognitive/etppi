package rtcw;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Comparator;
import java.util.Arrays;

import java.util.Calendar;
import java.util.GregorianCalendar;

class ETQuery implements Comparator
{

	public static final int KILLDEATHDELTA = 1;
	public static final int HEADSHOT = 2;
	public static final int DAMAGEGR = 3;
	public static final int DAMAGEGT = 4;

	public static final int BSENSE =	5;
	public static final int LWEAPONS =	6;
	public static final int HWEAPONS =	7;
	public static final int COVOPS =	8;
	public static final int SIGNALS =	9;
	public static final int ENGINEERING = 10;
	public static final int FIRSTAID =	11;

	public static final int COLT =	12;
	public static final int LUGER =	14;
	public static final int MP40  = 15;
	public static final int THOMPSON = 16;
	public static final int STEN = 17;
	public static final int FG42 = 18;
	public static final int GARAND = 19;
	public static final int K43 = 20;

	public static final int PANZER = 21;
	public static final int MG42 = 22;
	public static final int FTHROWER = 23;
	public static final int MORTAR = 24;

	public static final int KNIFE =		25;
	public static final int SATCHEL =	26;
	public static final int GRENADE =	27;
	public static final int GLAUNCHER = 28;
	public static final int LANDMINE =	29;
	public static final int DYNAMITE =	30;
	public static final int AIRSTRIKE = 31;
	public static final int ARTILLERY = 32;

	MsgOutput o = new MsgOutput( MsgOutput.ERROR,"RTCW.ETQuery" );
	Vector preparedValues = null;
	Vector preparedLegends = null;
	Vector preparedKeys = null;
	Vector preparedTeams = null;
	int weapDataType = 0;
	int skillDataType = 0;
	
	private long startDate = 0;
	private long endDate = 0;
	private Object [] rounds = null;
	private int index = 0;
	Vector  filteredRounds = new Vector();

	// Allows to order round by end date (ascending)
	public int compare (Object a, Object b) {	
		long vala = ((Round)a).get_endDate();
		long valb = ((Round)b).get_endDate();

		if (vala == valb) return 0;
		if (vala > valb) return 1;
		if (vala < valb) return -1;
		return 0;
	}

	// Query can be construct with String like 01/07/03
	public ETQuery ( String startUserString, String endUserString ) {
	
		String [] start = startUserString.split("/");
		String [] end   = endUserString.split("/");
		
		//System.out.println(start[2]+" "+start[1]+" "+start[0]);
		//System.out.println(end[2]+" "+end[1]+" "+end[0]);

		Calendar cal = new GregorianCalendar();

		// Year += 2000; Month -= 1 cos 01/07/03
		cal.set ( Integer.parseInt(start[2])+2000, Integer.parseInt(start[1])-1, Integer.parseInt(start[0]), 0, 0, 0 ); 
		this.startDate = cal.getTime().getTime() / 1000;
		cal.set ( Integer.parseInt(end[2])+2000, Integer.parseInt(end[1])-1, Integer.parseInt(end[0]), 23, 59, 59 ); 
		this.endDate = cal.getTime().getTime() / 1000;
	}


	public ETQuery ( long starttstp, long endtstp  ) {

		this.startDate = starttstp;
		this.endDate = endtstp;
		//System.out.println( "ETQuery : " + Long.toString(starttstp) +"-"+ Long.toString(endtstp) );
	}
	
	// Atomic operation - can not be cut in smaller piece
	// rounds are ordered by date asc 
	public int sort ( ) {
		rounds = Round.get_allRounds().toArray();
		Arrays.sort( rounds , this );
		return rounds.length;
	}


	// rounds are ordered by date asc 
	public boolean processNext ( int hardCodedLimit ) {

		if ( filteredRounds.size() >= hardCodedLimit) return false;
		o.debug("Hard Coded Round Max Limit: "+hardCodedLimit);

		// 2- round not matching [startdate,enddate] are not returned  
		int alength = rounds.length;

		if (index < alength) {
			long currEdate = ((Round)rounds[index]).get_endDate();
			//System.out.println( "round date is : " + Long.toString(currEdate)  );
			if ( currEdate >= startDate  && currEdate <= endDate ) {
				filteredRounds.add( rounds[index] );
			} else {
				//System.out.println( "round date is : " + Long.toString(currEdate)+ " skipped !!"  );
			}
			index++;
			return true;
		}
		return false;
	}
	

	// Allow to choose between Accuracy, Death or Kill for weapon display
	public void set_weapDataType ( int wSelector) {
		o.debug(" weapDataType = "+ wSelector);
		this.weapDataType = wSelector;
	}

	// Allow to choose between Points, Level or Medals for skill display
	public void set_skillDataType ( int sSelector) {
		o.debug(" skillDataType = "+ sSelector);
		this.skillDataType = sSelector;
	}

	public boolean prepareVectors ( int valueType ) {
	
		Enumeration lrounds = filteredRounds.elements();
		preparedValues = new Vector();
		preparedLegends = new Vector();
		preparedKeys = new Vector();
		preparedTeams = new Vector();
		
		while ( lrounds.hasMoreElements() ) {
			Round currRound = (Round)lrounds.nextElement();
			
			switch (valueType) {
				case KILLDEATHDELTA:	
					preparedValues.add( new Double(currRound.get_roundkill() - currRound.get_rounddeath()));
					break;  
				case HEADSHOT :
					preparedValues.add( new Double(currRound.get_roundhshot()));
					break;
				case DAMAGEGR :
					preparedValues.add( new Double((double)currRound.get_damageG() - (double)currRound.get_damageR()) );
					break;
				case DAMAGEGT :
					preparedValues.add( new Double((double)currRound.get_damageG() - (double)currRound.get_damageT()) );
					break;
				
				case BSENSE:
					preparedValues.add( new Double(currRound.get_skill(Skill.BATTLE_SENSE).get_value(skillDataType) ));
					break;
				case LWEAPONS:
					preparedValues.add( new Double(currRound.get_skill(Skill.LIGHT_WEAPONS).get_value(skillDataType) ) );
					break;
				case HWEAPONS:
					preparedValues.add( new Double(currRound.get_skill(Skill.HEAVY_WEAPONS).get_value(skillDataType) ) );
					break;
				case COVOPS:
					preparedValues.add( new Double(currRound.get_skill(Skill.COVERT_OPS).get_value(skillDataType) ) );
					break;
				case SIGNALS:
					preparedValues.add( new Double(currRound.get_skill(Skill.SIGNALS).get_value(skillDataType) ) );
					break;
				case ENGINEERING:
					preparedValues.add( new Double(currRound.get_skill(Skill.ENGINEERING).get_value(skillDataType) ) );
					break;
				case FIRSTAID:
					preparedValues.add( new Double(currRound.get_skill(Skill.FIRST_AID).get_value(skillDataType) ) );
					break;
				
				case COLT:
					preparedValues.add( new Double(currRound.get_weapon("Colt     ").get_value(weapDataType) ));
					break;
				case LUGER:
					preparedValues.add( new Double(currRound.get_weapon("Luger    ").get_value(weapDataType) ));
					break;
				case MP40:
					preparedValues.add( new Double(currRound.get_weapon("MP-40    ").get_value(weapDataType) ));
					break;
				case THOMPSON:
					preparedValues.add( new Double(currRound.get_weapon("Thompson ").get_value(weapDataType) ));
					break;
				case STEN:
					preparedValues.add( new Double(currRound.get_weapon("Sten     ").get_value(weapDataType) ));
					break;
				case FG42:
					preparedValues.add( new Double(currRound.get_weapon("FG-42    ").get_value(weapDataType) ));
					break;
				case GARAND:
					preparedValues.add( new Double(currRound.get_weapon("Garand   ").get_value(weapDataType) ));
					break;
				case K43:
					preparedValues.add( new Double(currRound.get_weapon("K43 Rifle").get_value(weapDataType) ));
					break;
				case PANZER:
					preparedValues.add( new Double(currRound.get_weapon("Panzer   ").get_value(weapDataType) ));
					break;
				case MG42:
					preparedValues.add( new Double(currRound.get_weapon("MG-42 Gun").get_value(weapDataType) ));
					break;
				case FTHROWER:
					preparedValues.add( new Double(currRound.get_weapon("F.Thrower").get_value(weapDataType) ));
					break;
				case MORTAR:
					preparedValues.add( new Double(currRound.get_weapon("Mortar   ").get_value(weapDataType) ));
					break;
				case KNIFE:
					preparedValues.add( new Double(currRound.get_weapon("Knife    ").get_value(weapDataType) ));
					break;
				case SATCHEL:
					preparedValues.add( new Double(currRound.get_weapon("Satchel  ").get_value(weapDataType) ));
					break;
				case GRENADE:
					preparedValues.add( new Double(currRound.get_weapon("Grenade  ").get_value(weapDataType) ));
					break;
				case GLAUNCHER:
					preparedValues.add( new Double(currRound.get_weapon("G.Launchr").get_value(weapDataType) ));
					break;
				case LANDMINE:
					preparedValues.add( new Double(currRound.get_weapon("Landmine ").get_value(weapDataType) ));
					break;
				case DYNAMITE:
					preparedValues.add( new Double(currRound.get_weapon("Dynamite ").get_value(weapDataType) ));
					break;
				case AIRSTRIKE:
					preparedValues.add( new Double(currRound.get_weapon("Airstrike").get_value(weapDataType) ));
					break;
				case ARTILLERY:
					preparedValues.add(  new Double( currRound.get_weapon("Artillery").get_value(weapDataType)) );
					break;

			}
			preparedLegends.add ( currRound.get_mapName() );
			preparedTeams.add ( currRound.get_teamName() );
			preparedKeys.add ( new Long(currRound.get_key()) );
			
		}
		return true;
	}

	public Vector get_valueVector () {
		return this.preparedValues;
	}
	
	public Vector get_legendVector () {
		return this.preparedLegends;
	}

	public Vector get_keyVector () {
		return this.preparedKeys;
	}

	public Vector get_teamVector () {
		return this.preparedTeams;
	}

	public int get_filteredRoundsSize () {
		return filteredRounds.size();
	}

}
