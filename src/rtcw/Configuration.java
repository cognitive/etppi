/*
 * Created on 20 juil. 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package rtcw;

import java.util.Hashtable;
import java.util.Enumeration;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.util.regex.Pattern;

/**
 * @author Administrateur
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class Configuration extends Hashtable {

	FileUtil fu = FileUtil.getInstance();
	MsgOutput o = new MsgOutput( MsgOutput.WARNING, "RTCW.Configuration" );
	
	String configurationFileName = new String("ETPPI.ini");
	String keyValueSeparator = "=";
	String kvs = keyValueSeparator;
	// ([^=])=(.*)$
	Pattern linePattern = PerlLike.CompilePattern("([^"+kvs+"]*)"+kvs+"(.*)$");	

	static Configuration _instance = null;
	
	static public Configuration getInstance() {

		if (_instance == null) {
			_instance = new Configuration();
			_instance.load();
		}
		return _instance;
	}
	

	public boolean setValue ( String key, String value ) {
		this.put(key, value);
		o.debug("key/value added: "+key+"/"+value);
		return true;
	}
	public String getValue (  String key) {
		return (String)this.get(key);
	}
	
	public int save () {
		int bytes= -1;
		boolean failure = false;
		
		if (fu.CheckFileWAccess(configurationFileName)) {
				
			BufferedWriter w = fu.GetWriter(configurationFileName);
			Enumeration allKeys = this.keys();
			while (allKeys.hasMoreElements() && !failure){

				String currKey = allKeys.nextElement().toString();
				String currVal = this.getValue(currKey);		
				
				int lastWrite = fu.writeLine(w, currKey+keyValueSeparator+currVal );
				if (lastWrite != -1) bytes += lastWrite;
				else { failure = true; bytes = -1; }
			}	
		
			if (fu.CloseWriter(w)) {
				o.info("Configuration written "+bytes+" bytes");
			}
		}
		return bytes;
	}

	public int load  () {
		String line = null;
		String key=null, value=null;
		
		if (fu.CheckFileRAccess(configurationFileName)) {
				
			BufferedReader w = fu.GetReader(configurationFileName);
			
			while (  (line = fu.readLine(w)) != null ) {
				key = 	PerlLike.BasicExtractionCP(line, linePattern, 1);
				value =	PerlLike.BasicExtractionCP(line, linePattern, 2);
				this.setValue(key,value);	
			}
			
		}
		return 0;
	}

}



