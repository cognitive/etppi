package rtcw;

class Weapon 
{
	public static final int SELECT_ACCURACY = 1;
	public static final int SELECT_KILL     = 2;
	public static final int SELECT_DEATH    = 3;

	public String weapName;
	double accuracy;
	String hits;
	int kill;
	int death;

	public Weapon( String weapName, double accu, String hits, int kill, int death ) {
		this.weapName = weapName;
		this.accuracy = accu;
		this.hits = hits;
		this.kill = kill;
		this.death = death;
		//System.out.println("new weapon "+weapName+":"+Double.toString(accu)+","+hits+","+Integer.toString(kill)+","+Integer.toString(death));
	}

	public double get_value ( int selector ) {
		
		if ( selector == SELECT_ACCURACY ) return this.accuracy;
		if ( selector == SELECT_KILL ) return this.kill;
		if ( selector == SELECT_DEATH ) return this.death;
		return 0.0;
	}

	public int get_kill() {
		return this.kill;
	}
	
	public double get_accuracy() {
		return this.accuracy;
	}
}

