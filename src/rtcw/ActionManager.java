package rtcw;

import java.awt.event.*;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import java.util.Vector;
import java.util.Enumeration;

import java.io.File;

public class ActionManager implements ActionListener
{

	static String primaryConfFName = "ETPPI.dat";	
	static String tagForSelectedFiles = "#SelectedFiles"; 

	
	MsgOutput o = new MsgOutput( MsgOutput.WARNING, this.getClass().getName() ); 
	MainFrame mf = null;
	JFileChooser fc = null;
	Vector filesVector = new Vector();
	Vector directoriesVector = new Vector();
	FileUtil fu = FileUtil.getInstance();
	QueryFrame queryFrame = null;
	
	// Constructor
	public ActionManager ( MainFrame mainFrame ) {
		this.mf = mainFrame; 
	}
	
	//
	// Window enabling/disabling
	//

	// disable main frame
	public void queryFrameOpening() {
		o.debug("Query frame opening - disable main frame");
		this.mf.enableWindow( false );
	}

	// enable main frame
	public void queryFrameClosing() {
		o.debug("Query frame closing - enable main frame");
		this.mf.enableWindow( true );
	}

	// disable query frame
	public void optionFrameOpening() {
		o.debug("Options frame opening - disable query frame");
		queryFrame.enableWindow( false );
	}
	
	// enable query frame
	public void optionFrameClosing() {
		o.debug("Options frame closing - enable query frame");
		queryFrame.enableWindow( true );
	}


	public void taskProgressChanged( int currValue,  int maxValue ) {
		mf.updateProgressBar (currValue, maxValue);
	}

	public void dataReadyForDisplay( ETQuery q) {
		JFrame frame = new JFrame("Display Statistic");
		ChartPanel cp = new ChartPanel();


		/* Finish setting up the frame, and show it. */
		frame.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
		
		frame.getContentPane().add( cp );
		frame.pack();
		frame.setVisible(true);
	}	
	
	public void windowShownFirstTime() {
		o.debug("Mainframe shown !! (first time)");
		Vector directoriesNamesVector = fu.getSettings( primaryConfFName, tagForSelectedFiles);
		int idx = 0;
		while (idx < directoriesNamesVector.size() ){
			directoriesVector.add( new File((String)directoriesNamesVector.elementAt(idx++)) );
		}	
		
		filesVector = fu.getFileVectorFromDirVector( directoriesVector );
		if ( directoriesVector.size() > 0 ) {
			this.mf.updateFileList( directoriesVector, filesVector );
			return;
		}
	}


	public void actionPerformed(ActionEvent e) {


		if ("load".equals(e.getActionCommand())) {
			o.debug("User clicked load");
			directoriesVector = fu.getSettings( primaryConfFName, tagForSelectedFiles );
			filesVector = fu.getFileVectorFromDirVector( directoriesVector );					
			this.mf.updateFileList( directoriesVector, filesVector );
			return;
		}

		// Stat directories selection
		if ("select".equals(e.getActionCommand())) {
			o.debug("process started.");
			o.debug("name =>"+mf.getName()+"<");
		
			if ( this.fc == null ) {
				this.fc = new JFileChooser();
				
				//fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fc.setMultiSelectionEnabled( true );
				fc.setApproveButtonText("Add these stat files");
			}

			int returnVal = fc.showOpenDialog(this.mf);
			
			// Now we work on directory
			//File [] selectedFiles = null;
			File [] selectedDirectories = null;
			
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				//selectedFiles = fc.getSelectedFiles();
				selectedDirectories = fc.getSelectedFiles();

				int idx=0;
				while ( idx < selectedDirectories.length ) {
						directoriesVector.add( selectedDirectories[idx++] );
				}		
				filesVector = fu.getFileVectorFromDirVector( directoriesVector );			
				this.mf.updateFileList( directoriesVector, filesVector );
				//fc.changeToParentDirectory();
			}
		
			return;
		}

		if ("clear".equals(e.getActionCommand())) {
			filesVector = new Vector();
			directoriesVector = new Vector();
			this.mf.updateFileList( directoriesVector, filesVector );
			return;
		}

		if ("save".equals(e.getActionCommand())) {
			o.debug("User clicked save");
			o.debug("Ready to save directory list:"+ directoriesVector.size());
			Vector directoryNames = new Vector();
			Enumeration enumeration = directoriesVector.elements();
			while (enumeration.hasMoreElements())	{
				File d = (File)enumeration.nextElement();
				directoryNames.add ( d.getAbsolutePath() );
			}
			
			fu.saveSettings( primaryConfFName, tagForSelectedFiles, directoryNames );
			return;
		}

		if ("parse".equals(e.getActionCommand())) {
			o.debug("User clicked parse");
			if (! fu.checkFiles( filesVector ) ) { return; }
			//pe.parseFiles(filesVector);

			// Hide the previous query Frame
			if (this.queryFrame != null) {
				queryFrame.setVisible(false);
				queryFrame.removeNotify();
				this.queryFrame = null;
			}

			SwingWorkerParser swp = new SwingWorkerParser();
			swp.setFilesToParse ( filesVector );
			swp.setActionManager( this );
			swp.start();

		}

	}

	public void parsingThreadFinished ( int numround ) {

		this.queryFrame= new QueryFrame("Query Window  "+ETStat.getAppName(), this);
		this.queryFrameOpening();
		
		queryFrame.pack();
		queryFrame.setVisible(true);
	}

}
