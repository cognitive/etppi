package rtcw;

import java.awt.event.*;
import javax.swing.JPanel;
import java.awt.Dimension;

import java.awt.GraphicsEnvironment;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import java.util.Locale;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;

// vendredi 18 juillet 2003

class FontSelectorPanel extends JPanel  implements ActionListener  {

	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment(); 
	String [] fontFamilies = ge.getAvailableFontFamilyNames(Locale.getDefault());
	Font initialFont = this.getFont();

	JComboBox fontList = new JComboBox();
	JCheckBox style = new JCheckBox("Bold", true);
	JCheckBox alias = new JCheckBox("AntiAlias", true);
	String str2display = " goldrush is my prefered map";
	JTextArea ta = new JTextArea(str2display,2,20); 
	boolean bold = true;

	public void setSelectedFont( String familyName ) {
		fontList.setSelectedItem((Object)familyName);
		refreshTextArea( familyName, bold);
	}
	public String getSelectedFont() {
		return (String)fontList.getSelectedItem();
	}

	public boolean getBold() {
		return bold;
	}
	public void setBold( boolean bold) {
		this.bold = bold;
		style.setSelected(bold);
		refreshTextArea( (String)fontList.getSelectedItem(), bold);
	}
	public boolean getAntiAlias() {
		return alias.isSelected();
	}
	public void setAntiAlias( boolean antialias) {
		alias.setSelected(antialias);
	}
	


	public FontSelectorPanel() {
		super ();
		this.setPreferredSize( new Dimension (300,100) );
		//this.setLayout(new FlowLayout());
		this.setLayout(new java.awt.GridLayout(2,2,15,15));
		ta.setEditable( false );
		
		add( style );
		add( fontList );
		
		add( alias );
		add( ta );
		ta.setBorder( BorderFactory.createBevelBorder(BevelBorder.LOWERED));
	

		// Initialize font list
		// Test if the font can display dummy string
		int cnter = 0;
		while (cnter < fontFamilies.length) {
			String currFamily = fontFamilies[cnter];
			Font newFont = new Font(currFamily, Font.PLAIN, 9); 
			if ( newFont.canDisplayUpTo(str2display) == -1 ) {
				fontList.addItem(currFamily);
			}
			cnter++;
		}

	
		fontList.addActionListener(this);
		style.addActionListener(this);
		ta.setFont( new Font("Dialog", Font.BOLD, 9) );
	}

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        
		if ( o instanceof JCheckBox) {
			bold = !bold;
		}
		String fontName = (String)fontList.getSelectedItem();
		refreshTextArea( fontName, bold);
	}

	private void refreshTextArea ( String fontName, boolean bold) {

		if (bold)	ta.setFont( new Font(fontName, Font.BOLD, 9)); 
		else	ta.setFont( new Font(fontName, Font.PLAIN, 9));
	}

}