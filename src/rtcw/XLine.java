package rtcw;

class XLine extends java.awt.geom.Line2D.Double {

	String _legend = null;

	public XLine ( double x1, double y1, double x2, double y2, String lgd ) 	{
	
		super ( x1, y1, x2, y2 );
		this._legend = lgd;
	}
}
