package rtcw;

// Frame related
import javax.swing.JFrame;

// Layout related
import javax.swing.BorderFactory;
import javax.swing.border.*;
import javax.swing.JPanel;
import javax.swing.BoxLayout;

// Widget related
import java.awt.event.*;
import java.awt.GridLayout;
import java.awt.Color;
import java.util.Vector;


class OptionsFrame extends JFrame implements ActionListener  
{
	//Border bevelLowered =		BorderFactory.createEmptyBorder();
	Configuration conf =		Configuration.getInstance();
	Border bevelLowered =		BorderFactory.createBevelBorder(BevelBorder.LOWERED);
//	TitledBorder ccpBorder =	BorderFactory.createTitledBorder(bevelLowered, "Chart Colors");
//	TitledBorder fspBorder =	BorderFactory.createTitledBorder(bevelLowered, "Font");
//	TitledBorder ncpBorder =	BorderFactory.createTitledBorder(bevelLowered, "Player Name");
	
	Border insideSpaceBorder=	BorderFactory.createEmptyBorder(8,12,8,12);

	Border ccpBorder =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(bevelLowered, "Chart Colors"),insideSpaceBorder);
	Border fspBorder =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(bevelLowered, "Font"),insideSpaceBorder);
	Border ncpBorder =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(bevelLowered, "Player Name"),insideSpaceBorder);
	Border acpBorder =	BorderFactory.createEmptyBorder(16,12,16,12);


	ColorChooserPanel ccp	= new ColorChooserPanel();
	FontSelectorPanel fsp	= new FontSelectorPanel();
	ApplyCancelPanel  acp	= new ApplyCancelPanel(this);
	NickConstraintPanel ncp = new NickConstraintPanel();

	public ActionManager am = null;

	public void actionPerformed(ActionEvent e) {
		
		if ("apply".equals(e.getActionCommand()) || "save".equals(e.getActionCommand())) {
			//System.out.println("action: "+e.getActionCommand());
					
			// Color Panel
			Vector colors= ccp.getColors();
			for (int i=0;i<8;i++) {
				Color currColor = (Color)colors.elementAt(i);
				conf.setValue("Color"+i,"0x"+Integer.toHexString(currColor.getRGB() & 0xffffff));
			}
			boolean paint_use_gradient = ccp.get_useGradient(); 
			conf.setValue("UseGradient",String.valueOf(paint_use_gradient));

			// Font Selector  Panel
			conf.setValue("UseBold",String.valueOf(fsp.getBold()));
			conf.setValue("Font",String.valueOf(fsp.getSelectedFont()));
			conf.setValue("AntiAlias",String.valueOf(fsp.getAntiAlias()));

			// Nick name
			conf.setValue("OnlyPName",ncp.getNickConstraint());

			
			if ( "save".equals(e.getActionCommand()) ) {
				conf.save();
			}
		}

		if ("reset".equals(e.getActionCommand())) {
			initColorPanel();
			initFontPanel();
		}
		
		if ("close".equals(e.getActionCommand())) {
			System.out.println("action: "+e.getActionCommand());
			this.dispose();
		}
		
	}

	// Init color panel with possible conf
	private void initColorPanel () {
			Vector colors = new Vector();
			if (!conf.isEmpty())	{
				for (int i=0;i<8;i++) {
					colors.add ( Color.decode(conf.getValue("Color"+i)));
				}
				ccp.setColors( colors );		

				String paint_use_gradient = conf.getValue("UseGradient");
				ccp.set_useGradient( Boolean.valueOf(paint_use_gradient).booleanValue() ); 
			}
	}

	// Init font selector panel with possible conf
	private void initFontPanel () {
			if (!conf.isEmpty())	{
				String use_bold =  conf.getValue("UseBold");
				fsp.setBold(Boolean.valueOf(use_bold).booleanValue());
				fsp.setSelectedFont(conf.getValue("Font"));
				String anti_alias = conf.getValue("AntiAlias");
				fsp.setAntiAlias(Boolean.valueOf(anti_alias).booleanValue()); 
			}
	}

	// Init nick panel with possible conf
	private void initNickPanel () {
			if (!conf.isEmpty())	{
				ncp.setNickConstraint( conf.getValue("OnlyPName") );
				//o.debug("OnlyPName = "+conf.getValue("OnlyPName"));
			}
	}



	public OptionsFrame ( String title, ActionManager am ) {
	
		super ( title );
		
		this.am = am;
		JPanel main = new JPanel();
		main.setLayout( new BoxLayout(main, BoxLayout.X_AXIS) );

		ccp.setBorder( ccpBorder );
		fsp.setBorder( fspBorder );
		ncp.setBorder( ncpBorder );
		acp.setBorder( acpBorder );

		initColorPanel();
		initFontPanel();
		initNickPanel();


		JPanel rightSubPanel = new JPanel();

		rightSubPanel.setLayout(new GridLayout(3,1));
		rightSubPanel.add( fsp );
		rightSubPanel.add( ncp );
		rightSubPanel.add( acp );
		

		main.add( ccp ); // color chooser
		main.add( rightSubPanel ); // right Sub Panel

		this.getContentPane().add( main );
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				OptionsFrame of = (OptionsFrame)e.getSource();
				of.am.optionFrameClosing( );
			}
		});

	}

}
