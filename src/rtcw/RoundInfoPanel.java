package rtcw;

import java.io.BufferedReader;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Font;

class RoundInfoPanel extends JPanel 
{
	Round round = null;
	
	public RoundInfoPanel( Round r) {

		this.round = r;
		
		FileUtil fu = FileUtil.getInstance();
		
		if (!fu.CheckFileRAccess ( r.get_fileName()) )	return;

		BufferedReader br = fu.GetReader( r.get_fileName() );
		if (br == null) return;

		String line = null;
		String textFile = new String("");
		while ( ( line =  fu.readLine( br ) ) != null ) {
			textFile += line;
			textFile += "\n";
		}
		fu.CloseReader( br );
		JTextArea ta = new JTextArea(textFile, 25, 40);
		ta.setFont (new Font("Monospaced", Font.PLAIN, 11));
		ta.setEditable( false );
		this.setLayout ( new java.awt.GridLayout(1,1));
		this.add( new JScrollPane(ta) );
	}
}
